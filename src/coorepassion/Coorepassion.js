let util = await use('concord.util')

let Bot = await use('concord.Bot')
let ChannelType = await use('concord.api.channel.Type')
let Activity = await use('concord.gw.Activity')
let ActivityType = await use('concord.gw.ActivityType')
let Gw = await use('concord.gw.Gw')
let Intent = await use('concord.gw.Intent')
let Presence = await use('concord.gw.Presence')
let PresenceStatus = await use('concord.gw.PresenceStatus')
let Enum = await use('concord.types.Enum')
let State = await use('concord.state.State')
let IndexedDB = await use('concord.storage.IndexedDB')

let coorepassion = await use('coorepassion')

class NodeType extends Enum
{
	// Used in nodeGen only
	static ORIGIN = -1
	// Not decided yet
	static UNKNOWN = 0
	// Internal node, a single child is selected by a player
	static CHOICE = 1
	// Internal node, a single child is selected randomly
	static DICE = 2
	// Leaf node (without children)
	static LEAF = 3
	// Internal node with title and/or content
	static LINEAR = 4
	// Internal node without title or content
	static PASSTHROUGH = 5
}

class NodeActionType extends Enum
{
	static CALL = 'call'               // <NodeState.id>
	// Goto resets the call stack
	static GOTO = 'goto'               // <NodeState.id>
	static ITEM_UNLOCK = 'item_unlock' // <ItemState.id>
	static RET = 'ret'
}

class NodeActionState extends State
{
	type

	constructor(type)
	{
		super()
		this.type = type
	}

	static from(type, ...args)
	{
		switch (type)
		{
			case NodeActionType.CALL: return NodeActionCallState.from(...args)
			case NodeActionType.GOTO: return NodeActionGotoState.from(...args)
			case NodeActionType.ITEM_UNLOCK: return NodeActionItemUnlockState.from(...args)
			case NodeActionType.RET: return NodeActionRetState.from(...args)
		}

		return null
	}

	asString()
	{
		return this.type
	}
}
State.register(NodeActionState)

class NodeActionCallState extends NodeActionState
{
	idNode

	constructor(id_node)
	{
		super(NodeActionType.CALL)
		this.idNode = id_node
	}

	static from(...args)
	{
		if (args.length !== 1)
			return null

		return new NodeActionCallState(...args)
	}

	asString()
	{
		return `${super.asString()} ${this.idNode}`
	}
}
State.register(NodeActionCallState)

class NodeActionGotoState extends NodeActionState
{
	idNode

	constructor(id_node)
	{
		super(NodeActionType.GOTO)
		this.idNode = id_node
	}

	static from(...args)
	{
		if (args.length !== 1)
			return null

		return new NodeActionGotoState(...args)
	}

	asString()
	{
		return `${super.asString()} ${this.idNode}`
	}
}
State.register(NodeActionGotoState)

class NodeActionItemUnlockState extends NodeActionState
{
	idItem

	constructor(id_item)
	{
		super(NodeActionType.ITEM_UNLOCK)
		this.idItem = id_item
	}

	static from(...args)
	{
		if (args.length !== 1)
			return null

		return new NodeActionItemUnlockState(...args)
	}

	asString()
	{
		return `${super.asString()} ${this.idItem}`
	}
}
State.register(NodeActionItemUnlockState)

class NodeActionRetState extends NodeActionState
{
	constructor()
	{
		super(NodeActionType.RET)
	}

	static from(...args)
	{
		if (args.length !== 0)
			return null

		return new NodeActionRetState(...args)
	}

	asString()
	{
		return super.asString()
	}
}
State.register(NodeActionRetState)

class NodeState extends State
{
	static ID_ROOT_NODE = '0'

	id
	type = NodeType.UNKNOWN
	idBase = null
	idParent = null
	idChildren = {} // { child.idBase: id }
	idNext = null
	acts = []
	customNextEmoji = null
	title
	content

	constructor(id, title, content)
	{
		super()
		this.id = id ?? ''
		this.title = title ?? ''
		this.content = content ?? ''
	}

	asString()
	{
		let title = this.title
		if (title !== '')
		title = ` : ${title}`

		let content = this.content.trim()
		if (content !== '')
			content = `\n${content.replace(/^/gm, '> ')}`

		let actions = this.acts.map(act => `> ${act.asString()}`).join('\n')
		if (actions !== '')
			actions = `\nActions :\n${actions}`

		return `${this.id} [${this.type}]${title}${content}${actions}`
	}
}
State.register(NodeState)

class ItemState extends State
{
	id
	name = ''
	unlocked = false

	constructor(id)
	{
		super()
		this.id = id
	}
}
State.register(ItemState)

class ExecState extends State
{
	idCurNode
	idxAct // The index of the *next* action to be executed
	choice

	constructor(id_cur_node, idx_act, choice)
	{
		super()
		this.reset(id_cur_node, idx_act, choice)
	}

	reset(id_cur_node, idx_act, choice)
	{
		this.idCurNode = id_cur_node ?? NodeState.ID_ROOT_NODE
		this.idxAct = idx_act ?? 0
		this.choice = choice ?? null
	}

	asString()
	{
		let choice = ''
		if (this.choice !== null)
			choice = `, Choix : '${this.choice}'`

		return `Nœud : '${this.idCurNode}', Action : #${this.idxAct}${choice}`
	}
}
State.register(ExecState)

class PlayerState extends State
{
	id
	idTeam
	movementPoints = 0
	lastRefill = null
	execStack = [] // ExecState[]

	constructor(id, id_team)
	{
		super()
		this.id = id
		this.idTeam = id_team
	}

	asString()
	{
		let exec_stack = this.execStack.map((exec_state, idx) => `> #${idx} : ${exec_state.asString()}`).join('\n')
		if (exec_stack !== '')
			exec_stack = `\nÉtats :\n${exec_stack}`

		return `<@${this.id}> :\nTeam : <#${this.idTeam}>\nPM : ${this.movementPoints}${exec_stack}`
	}
}
State.register(PlayerState)

class TeamState extends State
{
	id
	name
	items = {} // { id: ItemState }

	constructor(id_channel, name)
	{
		super()
		this.id = id_channel ?? ''
		this.name = name ?? ''
	}

	asString()
	{
		return `${this.name} <#${this.id}>`
	}
}
State.register(TeamState)

class EventWireType extends Enum
{
	static PLAYER_ACTION_NEXT = 0
}

// A wire binds several things together
class EventWireState extends State
{
	type
	fired = false

	constructor(type)
	{
		super()
		this.type = type
	}
}
State.register(EventWireState)

class EventWirePlayerActionNextState extends EventWireState
{
	static _index = 'idMsg'

	idGame   // GameState.id
	idPlayer // PlayerState.id
	idChannel
	idMsg

	constructor(id_game, id_player, id_channel, id_msg)
	{
		super(EventWireType.PLAYER_ACTION_NEXT)
		this.idGame = id_game
		this.idPlayer = id_player
		this.idChannel = id_channel
		this.idMsg = id_msg
	}
}
State.register(EventWirePlayerActionNextState)

class EventWireManagerState extends State
{
	wires = {}

	getAll(type, ...idxs)
	{
		let wires = this.wires[type] ?? null
		if (wires === null)
			wires = {}

		if (idxs.length === 0)
			return Object.values(wires)

		return idxs.map(idx => wires[idx] ?? null)
	}

	filter(type, fields)
	{
		let wires = this.wires[type] ?? null
		if (wires === null)
			return []

		let ks = Object.keys(fields)
		return Object.values(wires).filter(w => ks.every(k => fields[k] === w[k]))
	}

	filterIndex(type, fields)
	{
		let wires = this.wires[type] ?? null
		if (wires === null)
			return []

		let ks = Object.keys(fields)
		return Object.keys(wires).filter(idx => ks.every(k => fields[k] === wires[idx][k]))
	}

	putAll(...wrs)
	{
		for (let i in wrs)
		{
			let wire = wrs[i]
			if (!(wire instanceof EventWireState))
				throw new Error(`Parameter ${i} is not an instance of EventWireState`)

			let wires = this.wires[wire.type] ?? {}

			wires[wire[wire.constructor._index]] = wire
			this.wires[wire.type] = wires
		}
	}

	removeAll(type, ...idxs)
	{
		if (idxs.length === 0)
			return

		let wires = this.wires[type] ?? null
		if (wires === null)
			return

		for (let idx of idxs)
			delete wires[idx]
	}
}
State.register(EventWireManagerState)

class GameState extends State
{
	static CALL_STACK_LIMIT = 128

	id
	idAdminChannel = null
	idImpersonationChannels = []
	nodes = {}   // { id: NodeState }
	players = {} // { id: PlayerState }
	teams = {}   // { id: TeamState }
	nodesValidated = false
	nextEmoji = '⬇️'
	enabled = false
	refill = 5
	refillSplits = [ [0, 0, 0, 0] ]

	constructor(id_guild)
	{
		super()
		this.id = id_guild
	}
}
State.register(GameState)

class GlobalState extends State
{
	eventWires = new EventWireManagerState() // EventWireManagerState
}
State.register(GlobalState)

class CoorepassionGw extends Gw
{
	bot

	constructor(bot, token)
	{
		super(token)

		this.intents = Intent.DIRECT_MESSAGES
				| Intent.DIRECT_MESSAGE_REACTIONS
				| Intent.GUILDS
				| Intent.GUILD_MESSAGES
				| Intent.GUILD_MESSAGE_REACTIONS
		this.bot = bot
	}

	onChannelCreate(chan)
	{
		super.onChannelCreate(chan)
		this.bot.handleChannelCreate(chan)
	}

	onChannelDelete(chan)
	{
		super.onChannelDelete(chan)
		this.bot.handleChannelDelete(chan)
	}

	onChannelUpdate(chan)
	{
		super.onChannelUpdate(chan)
		this.bot.handleChannelUpdate(chan)
	}

	onGuildCreate(guild)
	{
		super.onGuildCreate(guild)
		this.bot.handleGuildCreate(guild)
	}

	onMessageCreate(msg)
	{
		super.onMessageCreate(msg)
		this.bot.handleMessageCreate(msg)
	}

	onReady(payload)
	{
		super.onReady(payload)
		this.bot.handleReady(payload.user)
	}

	onMessageReactionAdd(react)
	{
		super.onMessageReactionAdd(react)
		this.bot.handleMessageReactionAdd(react)
	}
}

class Coorepassion extends Bot
{
	nextNodeMaxDurationMs = 100

	state = null    // GlobalState
	gameStates = {} // { id: GameState }
	storage
	user = null
	guilds = {}

	constructor(token)
	{
		super(token)
window.inst = this
		this.gw = new CoorepassionGw(this, token)
		this.storage = new IndexedDB('Coorepassion://')
	}

	async boot()
	{
		await this.loadState()
		await this.saveState()
		super.boot()
	}

	async loadState()
	{
		this.state = null

		return await this.storage.getItemOrElse('State', (new GlobalState()).serialize()).then(state => {
			this.state = State.unserialize(state)
		})
	}

	async saveState()
	{
		return await this.storage.setItem('State', this.state.serialize())
	}

	async loadGameState(id_guild)
	{
		this.gameStates[id_guild] = null

		return await this.storage.getItemOrElse(`GameState[${id_guild}]`, (new GameState(id_guild)).serialize()).then(gstate => {
			this.gameStates[id_guild] = State.unserialize(gstate)
		})
	}

	async saveGameState(id_guild)
	{
		return await this.storage.setItem(`GameState[${id_guild}]`, this.gameStates[id_guild].serialize())
	}

	checkOwner(id_guild, id_user)
	{
		let guild = this.guilds[id_guild] ?? null
		if (guild === null)
			return

		return id_user === guild.owner_id
	}

	gstateFromAdminChannel(id_guild, id_channel)
	{
		let gstate = this.gameStates[id_guild] ?? null
		if (gstate !== null)
		{
			if (id_channel === gstate.idAdminChannel)
				return gstate
		}

		gstate = Object.values(this.gameStates).find(gstate => gstate.idImpersonationChannels.includes(id_channel)) ?? null
		if (gstate !== null)
			return gstate

		return null
	}

	checkAdminChannel(id_guild, id_channel)
	{
		return this.gstateFromAdminChannel(id_guild, id_channel) !== null
	}

	handleReady(user)
	{
		this.user = user
		this.gw.updPresence(new Presence(PresenceStatus.ONLINE, false, null, new Activity(ActivityType.GAME, 'foie gras-souillé')))
	}

	handleChannelCreate(chan)
	{
		this.guilds[chan.guild_id].channels.push(chan)
	}

	handleChannelDelete(chan)
	{
		let guild = this.guilds[chan.guild_id]
		let chan_idx = guild.channels.findIndex(e => e.id === chan.id)
		delete guild.channels[chan_idx]
	}

	handleChannelUpdate(chan)
	{
		let guild = this.guilds[chan.guild_id]
		let chan_idx = guild.channels.findIndex(e => e.id === chan.id)
		guild.channels[chan_idx] = chan
	}

	async handleGuildCreate(guild)
	{
		this.guilds[guild.id] = guild

		let gstate = this.gameStates[guild.id] ?? null
		if (gstate === null)
			await this.loadGameState(guild.id)
	}

	async handleCommandAdmin(msg, args)
	{
		if (!this.checkAdminChannel(msg.guild_id, msg.channel_id))
			return

		return this.handleSubCommand(msg, args, {
			'backup': this.handleCommandAdminBackup,
			'disable': this.handleCommandAdminDisable,
			'enable': this.handleCommandAdminEnable,
			'export': this.handleCommandAdminExport
		})
	}

	async handleCommandAdminBackup(msg, args)
	{
		if (args.trim() !== '')
			return

		let gstate = this.gstateFromAdminChannel(msg.guild_id, msg.channel_id)

		let backup = new File([ gstate.serialize() ], 'backup.txt', { type: 'text/plain' })

		this.rest.createEmbedMessage(msg.channel_id,
				'Sauvegarde',
				`Conservez-la à l'abri bien au chaud !`,
				null,
				null,
				backup)
	}

	async handleCommandAdminExport(msg, args)
	{
		if (args.trim() !== '')
			return

		let gstate = this.gstateFromAdminChannel(msg.guild_id, msg.channel_id)

		let ngs = new GameState(null)
		ngs.nodes = gstate.nodes
		ngs.nextEmoji = gstate.nextEmoji
		ngs.refill = gstate.refill
		ngs.refillSplits = gstate.refillSplits

		let backup = new File([ ngs.serialize() ], 'export.txt', { type: 'text/plain' })

		this.rest.createEmbedMessage(msg.channel_id,
				'Exportation',
				`Conservez-la à l'abri bien au chaud !`,
				null,
				null,
				backup)
	}

	async handleCommandAdminDisable(msg, args)
	{
		if (args.trim() !== '')
			return

		let gstate = this.gstateFromAdminChannel(msg.guild_id, msg.channel_id)

		gstate.enabled = false
		await this.saveGameState(gstate.id)

		this.rest.createEmbedMessage(msg.channel_id,
				'Partie désactivée')
	}

	async handleCommandAdminEnable(msg, args)
	{
		if (args.trim() !== '')
			return

		let gstate = this.gstateFromAdminChannel(msg.guild_id, msg.channel_id)

		gstate.enabled = true
		await this.saveGameState(gstate.id)

		this.rest.createEmbedMessage(msg.channel_id,
				'Partie activée')
	}

	async handleCommandBoot(msg, args)
	{
		if (!this.checkOwner(msg.guild_id, msg.author.id))
			return

		if (args.trim() !== '')
			return

		let id_game = msg.guild_id
		let gstate = this.gameStates[id_game]
		gstate.idAdminChannel = msg.channel_id
		await this.saveGameState(id_game)

		this.rest.createEmbedMessage(msg.channel_id,
				'Initialisation',
				`Salon administrateur défini à <#${msg.channel_id}>`)
	}

	async handleCommandImpersonate(msg, args)
	{
		let gstate = this.gstateFromAdminChannel(msg.guild_id, msg.channel_id)
		if (gstate === null)
			return

		let m = args.match(/^<#(\d+)>$/)
		if (m === null)
			return

		let id_channel = m[1]

		if (Object.values(this.gameStates).find(gs => gs.idImpersonationChannels.includes(id_channel)) !== undefined)
			return

		gstate.idImpersonationChannels.push(id_channel)
		await this.saveGameState(gstate.id)

		this.rest.createEmbedMessage(msg.channel_id,
				`Initialisation`,
				`Salon alternatif <#${id_channel}> assigné à l'administration de <#${msg.channel_id}>`)

		this.rest.createEmbedMessage(id_channel,
				`Initialisation`,
				`Salon alternatif <#${id_channel}> assigné à l'administration de <#${msg.channel_id}>`)
	}

	async handleCommandNode(msg, args)
	{
		if (!this.checkAdminChannel(msg.guild_id, msg.channel_id))
			return

		return this.handleSubCommand(msg, args, {
			'act': this.handleCommandNodeAct,
			'add': this.handleCommandNodeAdd,
			'del': this.handleCommandNodeDel,
			'find': this.handleCommandNodeFind,
			'gen': this.handleCommandNodeGen,
			'list': this.handleCommandNodeList,
			'mv': this.handleCommandNodeMv,
			'purge': this.handleCommandNodePurge,
			'replace': this.handleCommandNodeReplace,
			'show': this.handleCommandNodeShow
		})
	}

	async handleCommandNodeAct(msg, args)
	{
		return this.handleSubCommand(msg, args, {
			'list': this.handleCommandNodeActList, // TODO
			'set': this.handleCommandNodeActSet
		})
	}

	async handleCommandNodeActSet(msg, args)
	{
		let m = args.match(/^(\S+) *(?:\n([\S\s]*))?$/)
		if (m === null)
			return

		let id = m[1]
		let actions = m[2] ?? ''

		let gstate = this.gstateFromAdminChannel(msg.guild_id, msg.channel_id)

		let node = gstate.nodes[id] ?? null
		if (node === null)
			return

		let acts = []

		actions = actions.split('\n')
		for (let action of actions)
		{
			action = action.trim()
			if (action === '')
				continue

			let m = action.match(/^(\w+)(?: +(.+))*$/)
			if (m === null)
				return

			let type = m[1]
			let args = m[2] ?? ''

			let act = NodeActionState.from(type, args)
			if (act === null)
				return

			acts.push(act)
		}

		node.acts = acts
		await this.saveGameState(gstate.id)

		this.rest.createEmbedMessage(msg.channel_id,
				`Ajout d'actions`,
				`Actions définies pour le nœud :\n${node.asString()}`)
	}

	async handleCommandNodeAdd(msg, args)
	{
		let m = args.match(/^([^\s.]+(?:\.[^\s.]+)*)(?: +(.+))? *(?:\n([\S\s]*))?$/)
		if (m === null)
			return

		let id = m[1]
		let title = (m[2] ?? '').trim()
		let content = (m[3] ?? '').trim()

		let gstate = this.gstateFromAdminChannel(msg.guild_id, msg.channel_id)

		let node = gstate.nodes[id] ?? null
		if (node !== null)
			return

		node = new NodeState(id, title, content)
		gstate.nodes[id] = node
		await this.saveGameState(gstate.id)

		this.rest.createEmbedMessage(msg.channel_id,
				'Ajout de nœud',
				node.asString())
	}

	async handleCommandNodeDel(msg, args)
	{
		let m = args.match(/^(\S+)$/)
		if (m === null)
			return

		let id = m[1]

		let gstate = this.gstateFromAdminChannel(msg.guild_id, msg.channel_id)

		let node = gstate.nodes[id] ?? null
		if (node === null)
			return

		delete gstate.nodes[id]
		await this.saveGameState(gstate.id)

		this.rest.createEmbedMessage(msg.channel_id,
				'Suppression de nœud',
				`Nœud ${node.id} supprimé`)
	}

	async handleCommandNodeFind(msg, args)
	{
		let m = args.match(/^(\S+)$/)
		if (m === null)
			return

		let partial_id = m[1] ?? null

		let gstate = this.gstateFromAdminChannel(msg.guild_id, msg.channel_id)

		let ans = Object.values(gstate.nodes)
				.filter(node => node.id.includes(partial_id))
				.sort((a, b) => a.id < b.id ? -1 : a.id === b.id ? 0 : 1)
				.reduce((accum, node) => `${accum}${node.asString()}\n\n`, '')

		if (ans === '')
			ans = '*Vide*'

		this.rest.createEmbedMessageAutoSplit(msg.channel_id,
				'Liste des nœuds',
				ans)
	}

	async handleCommandNodeGen(msg, args)
	{
		if (args.trim() !== '')
			return

		let gstate = this.gstateFromAdminChannel(msg.guild_id, msg.channel_id)

		try
		{
			await this.nodeGen(gstate.id)

			this.rest.createEmbedMessage(msg.channel_id,
					'Génération',
					'Génération terminée avec succès')
		}
		catch (e)
		{
			this.rest.createEmbedMessage(msg.channel_id,
					'Onon',
					e.message)
		}
	}

	async handleCommandNodeList(msg, args)
	{
		if (args.trim() !== '')
			return

		let gstate = this.gstateFromAdminChannel(msg.guild_id, msg.channel_id)

		let ans = Object.values(gstate.nodes)
				.sort((a, b) => a.id < b.id ? -1 : a.id === b.id ? 0 : 1)
				.reduce((accum, node) => `${accum}${node.asString()}\n\n`, '')

		if (ans === '')
			ans = '*Vide*'

		this.rest.createEmbedMessageAutoSplit(msg.channel_id,
				'Liste des nœuds',
				ans)
	}

	async handleCommandNodeMv(msg, args)
	{
		let m = args.match(/^(\S+) +(\S+)$/)
		if (m === null)
			return

		let old_id = m[1]
		let new_id = m[2]

		if (old_id === new_id)
		{
			this.rest.createEmbedMessage(msg.channel_id,
					'Renommage de nœuds',
					'*Rien à faire*')
			return
		}

		let gstate = this.gstateFromAdminChannel(msg.guild_id, msg.channel_id)

		let nodes = Object.values(gstate.nodes)
				.filter(n => n.id.startsWith(old_id))

		let ans = ''

		for (let node of nodes)
		{
			let oid = node.id
			let nid = oid.replace(old_id, new_id)

			node.id = nid

			gstate.nodes[nid] = node
			delete gstate.nodes[oid]

			ans += `${oid} => ${nid}\n`
		}

		await this.saveGameState(gstate.id)

		if (ans === '')
			ans = '*Vide*'

		this.rest.createEmbedMessageAutoSplit(msg.channel_id,
				'Renommage de nœuds',
				ans)
	}

	async handleCommandNodePurge(msg, args)
	{
		if (args.trim() !== '')
			return

		let gstate = this.gstateFromAdminChannel(msg.guild_id, msg.channel_id)
		gstate.nodes = {}
		await this.saveGameState(gstate.id)

		this.rest.createEmbedMessage(msg.channel_id,
				'Suppression des nœuds',
				`Nœuds supprimés`)
	}

	async handleCommandNodeReplace(msg, args)
	{
		let m = args.match(/^(\S+) +(\S+)$/)
		if (m === null)
			return

		let str = m[1]
		let rpl = m[2]

		if (str === rpl)
		{
			this.rest.createEmbedMessage(msg.channel_id,
					'Renommage de nœuds',
					'*Rien à faire*')
			return
		}

		let gstate = this.gstateFromAdminChannel(msg.guild_id, msg.channel_id)

		let nodes = Object.values(gstate.nodes)
				.filter(n => n.id.includes(str))

		let ans = ''

		for (let node of nodes)
		{
			let oid = node.id
			let nid = oid.replaceAll(str, rpl)

			node.id = nid

			gstate.nodes[nid] = node
			delete gstate.nodes[oid]

			ans += `${oid} => ${nid}\n`
		}

		await this.saveGameState(gstate.id)

		if (ans === '')
			ans = '*Vide*'

		this.rest.createEmbedMessageAutoSplit(msg.channel_id,
				'Renommage de nœuds',
				ans)
	}

	async handleCommandNodeShow(msg, args)
	{
		let m = args.match(/^(\S+)$/)
		if (m === null)
			return

		let id = m[1]

		let gstate = this.gstateFromAdminChannel(msg.guild_id, msg.channel_id)

		let node = gstate.nodes[id] ?? null
		if (node === null)
			return this.rest.createEmbedMessage(msg.channel_id,
					'Onon',
					`Le nœud '${id}' n'existe pas`)

		return this.rest.createEmbedMessage(msg.channel_id,
				'Nœud',
				node.asString())
	}

	async handleCommandPlayer(msg, args)
	{
		if (!this.checkAdminChannel(msg.guild_id, msg.channel_id))
			return

		return this.handleSubCommand(msg, args, {
			'add': this.handleCommandPlayerAdd,
			'del': this.handleCommandPlayerDel,
			'execstate': this.handleCommandPlayerExecState,
			'list': this.handleCommandPlayerList,
			'mp': this.handleCommandPlayerMP,
			'show': this.handleCommandPlayerShow
		})
	}

	async handleCommandPlayerAdd(msg, args)
	{
		let m = args.match(/^<@!?(\d+)> +<#(\d+)>$/)
		if (m === null)
			return

		let id_player = m[1]
		let id_channel = m[2]

		let gstate = this.gstateFromAdminChannel(msg.guild_id, msg.channel_id)

		let team = gstate.teams[id_channel] ?? null
		if (team === null)
			return

		let player = gstate.players[id_player] ?? null
		if (player !== null)
			return

		player = new PlayerState(id_player, team.id)
		gstate.players[id_player] = player

		// Setup initial exec state
		this.playerStepNext(gstate.id, player.id)

		await this.saveGameState(gstate.id)

		this.rest.createEmbedMessage(msg.channel_id,
				'Ajout de joueur',
				`<@${player.id}> ajouté à l'équipe ${team.name}`)
	}

	async handleCommandPlayerDel(msg, args)
	{
		let m = args.match(/^<@!?(\d+)>$/)
		if (m === null)
			return

		let id_player = m[1]

		let gstate = this.gstateFromAdminChannel(msg.guild_id, msg.channel_id)

		let player = gstate.players[id_player] ?? null
		if (player === null)
			return

		delete gstate.players[id_player]
		await this.saveGameState(gstate.id)

		this.rest.createEmbedMessage(msg.channel_id,
				'Suppression de joueur',
				`<@${player.id}> supprimé`)
	}

	async handleCommandPlayerList(msg, args)
	{
		if (args.trim() !== '')
			return

		let gstate = this.gstateFromAdminChannel(msg.guild_id, msg.channel_id)

		let ans = Object.values(gstate.players)
				.sort((a, b) => a.idTeam < b.idTeam ? -1 : a.idTeam === b.idTeam ?
						(a.id < b.id ? -1 : a.id === b.id ? 0 : 1) : 1)
				.reduce((accum, player) => `${accum}${player.asString()}\n\n`, '')

		if (ans === '')
			ans = '*Vide*'

		this.rest.createEmbedMessageAutoSplit(msg.channel_id,
				'Liste des joueurs',
				ans)
	}

	async handleCommandPlayerShow(msg, args)
	{
		let m = args.match(/^<@!?(\d+)>$/)
		if (m === null)
			return

		let id_player = m[1]

		let gstate = this.gstateFromAdminChannel(msg.guild_id, msg.channel_id)

		let player = gstate.players[id_player] ?? null
		if (player === null)
			return

		this.rest.createEmbedMessage(msg.channel_id,
				'Joueur',
				player.asString())
	}

	async handleCommandPlayerMP(msg, args)
	{
		return this.handleSubCommand(msg, args, {
			'set': this.handleCommandPlayerMPSet
		})
	}

	async handleCommandPlayerMPSet(msg, args)
	{
		let m = args.match(/^<@!?(\d+)> +(\d+)$/)
		if (m === null)
			return

		let id_player = m[1]
		let mp = m[2] | 0

		let gstate = this.gstateFromAdminChannel(msg.guild_id, msg.channel_id)

		let player = gstate.players[id_player] ?? null
		if (player === null)
			return

		player.movementPoints = mp
		await this.saveGameState(gstate.id)

		this.rest.createEmbedMessage(msg.channel_id,
				'Modification de joueur',
				`Points de mouvement de <@${player.id}> définis à ${player.movementPoints}\n\n${player.asString()}`)
	}

	async handleCommandPlayerExecState(msg, args)
	{
		return this.handleSubCommand(msg, args, {
			'del': this.handleCommandPlayerExecStateDel,
			'set': this.handleCommandPlayerExecStateSet
		})
	}

	async handleCommandPlayerExecStateDel(msg, args)
	{
		let m = args.match(/^<@!?(\d+)> +(\d+)$/)
		if (m === null)
			return

		let id_player = m[1]
		let idx_exec_state = m[2] | 0

		let gstate = this.gstateFromAdminChannel(msg.guild_id, msg.channel_id)

		let player = gstate.players[id_player] ?? null
		if (player === null)
			return

		let exec_state = player.execStack[idx_exec_state] ?? null
		if (exec_state === null)
			return

		player.execStack.splice(idx_exec_state, 1)

		return this.rest.createEmbedMessage(msg.channel_id,
				'Modification de joueur',
				`État #${idx_exec_state} de <@${player.id}> supprimé\n\n${player.asString()}`)
	}

	async handleCommandPlayerExecStateSet(msg, args)
	{
		let m = args.match(/^<@!?(\d+)> +(\d+)(?: +(\S+)(?: +(\d+)(?: +(\S+))?)?)?$/)
		if (m === null)
			return

		let id_player = m[1]
		let idx_exec_state = m[2] | 0
		let id_node = m[3] ?? NodeState.ID_ROOT_NODE
		let idx_act = (m[4] ?? 0) | 0
		let choice = m[5] ?? null

		let gstate = this.gstateFromAdminChannel(msg.guild_id, msg.channel_id)

		let player = gstate.players[id_player] ?? null
		if (player === null)
			return

		let node = gstate.nodes[id_node] ?? null
		if (node === null)
			return

		if (choice !== null)
			choice = choice.replace(/^<a?:(\w+:\d+)>$/, '$1')

		let exec_state = new ExecState(node.id, idx_act, choice)
		player.execStack[idx_exec_state] = exec_state
		await this.saveGameState(gstate.id)

		this.rest.createEmbedMessage(msg.channel_id,
				'Modification de joueur',
				`État #${idx_exec_state} de <@${player.id}> défini à : ${exec_state.asString()}\n\n${player.asString()}`)
	}

	async handleCommandTeam(msg, args)
	{
		if (!this.checkAdminChannel(msg.guild_id, msg.channel_id))
			return

		return this.handleSubCommand(msg, args, {
			'add': this.handleCommandTeamAdd,
			'del': this.handleCommandTeamDel,
			'list': this.handleCommandTeamList,
			'purge': this.handleCommandTeamPurge
		})
	}

	async handleCommandTeamAdd(msg, args)
	{
		let m = args.match(/^<#(\d+)> +(.+)$/)
		if (m === null)
			return

		let id_channel = m[1]
		let name = m[2] ?? ''

		let gstate = this.gstateFromAdminChannel(msg.guild_id, msg.channel_id)

		let guild = this.guilds[gstate.id]
		if (!guild.channels.some(chan => chan.id === id_channel))
			return

		let team = gstate.teams[id_channel] ?? null
		if (team !== null)
			return

		team = new TeamState(id_channel, name)
		gstate.teams[id_channel] = team
		await this.saveGameState(gstate.id)

		this.rest.createEmbedMessage(msg.channel_id,
				`Ajout d'équipe`,
				`Nouvelle équipe :\n${team.asString()}`)
	}

	async handleCommandTeamDel(msg, args)
	{
		let m = args.match(/^<#(\d+)>$/)
		if (m === null)
			return

		let id_channel = m[1]

		let gstate = this.gstateFromAdminChannel(msg.guild_id, msg.channel_id)

		let team = gstate.teams[id_channel] ?? null
		if (team === null)
			return

		let players = Object.values(gstate.players).filter(p => p.idTeam === team.id)
		for (let player of players)
			delete gstate.players[player.id]

		delete gstate.teams[team.id]
		await this.saveGameState(gstate.id)

		this.rest.createEmbedMessage(msg.channel_id,
				`Suppression d'équipe`,
				`Équipe ${team.name} supprimée`)
	}

	async handleCommandTeamList(msg, args)
	{
		if (args.trim() !== '')
			return

		let gstate = this.gstateFromAdminChannel(msg.guild_id, msg.channel_id)

		let ans = Object.values(gstate.teams)
				.sort((a, b) => a.name < b.name ? -1 : a.name === b.name ? 0 : 1)
				.reduce((accum, team) => `${accum}${team.asString()}\n\n`, '')

		if (ans === '')
			ans = '*Vide*'

		this.rest.createEmbedMessageAutoSplit(msg.channel_id,
				'Liste des équipes',
				ans)
	}

	async handleCommandTeamPurge(msg, args)
	{
		if (args.trim() !== '')
			return

		let gstate = this.gstateFromAdminChannel(msg.guild_id, msg.channel_id)
		gstate.players = {}
		gstate.teams = {}
		await this.saveGameState(gstate.id)

		this.rest.createEmbedMessage(msg.channel_id,
				'Suppression des équipes',
				`Équipes supprimées`)
	}

	handleUnknownCommand(msg, args)
	{
		if (!this.checkAdminChannel(msg.guild_id, msg.channel_id)
				&& !this.checkOwner(msg.guild_id, msg.author.id))
			return

		this.rest.createEmbedMessage(msg.channel_id,
				'Onon',
				'Commande inconnue')
	}

	async handleSubCommand(msg, args, map)
	{
		let m = args.match(/^(\S+)(?: +([\S\s]+))?$/)
		if (m === null)
			return this.handleUnknownCommand(msg, args)

		let cmd = m[1]
		args = m[2] ?? ''

		let handler = map[cmd] ?? null
		if (handler === null)
			return this.handleUnknownCommand(msg, args)

		return handler.call(this, msg, args)
	}

	handleCommand(msg)
	{
		let d = msg.content ?? null
		if (d === null)
			return

		d = d.trim()
		if (d === '')
			return

		let m = d.match(/^`?<@!?(\d+)> +([\S\s]*[^`])`?$/)
		if (m === null)
			return

		if (m[1] !== this.user.id)
			return

		let args = m[2]

		return this.handleSubCommand(msg, args, {
			'admin': this.handleCommandAdmin,
			'boot': this.handleCommandBoot,
			'impersonate': this.handleCommandImpersonate,
			'node': this.handleCommandNode,
			'player': this.handleCommandPlayer,
			'team': this.handleCommandTeam
		})
	}

	async handleDMCommand(msg)
	{
		let id_player = msg.author.id

		let d = msg.content ?? null
		if (d === null)
			return

		d = d.trim()
		if (d === '')
			return

		let m = d.match(/^`?<@!?(\d+)>(?: +(\d+))?`?$/)
		if (m === null)
			return

		if (m[1] !== this.user.id)
			return

		let id_game = m[2] ?? null

		if (id_game === null)
		{
			let gstates = Object.values(this.gameStates).filter(gs => Object.keys(gs.players).findIndex(id => id === id_player) >= 0)

			if (gstates.length === 0)
				return this.rest.createEmbedMessage(msg.channel_id,
						'Onon',
						`Aucune partie en cours`)

			if (gstates.length > 1)
			{
				let cmds = gstates.map(gs => `> ${gs.teams[gs.players[id_player].idTeam].name} sur ${this.guilds[gs.id].name} : <@${this.user.id}> ${gs.id}`).join('\n')
				return this.rest.createEmbedMessage(msg.channel_id,
						'Onon',
						`Plusieurs parties en cours. Choisissez avec une des commandes suivantes :\n${cmds}`)
			}

			id_game = gstates[0].id
		}

		let gstate = this.gameStates[id_game] ?? null
		if (gstate === null)
			return this.rest.createEmbedMessage(msg.channel_id,
						'Onon',
						`Partie '${id_game}' inconnue`)

		if (!gstate.enabled)
			return

		this.showPlayerStory(id_game, id_player, msg.channel_id)
	}

	handleMessageCreate(msg)
	{
		if (msg.author.id === this.user.id)
			return

		msg.guild_id ??= null
		if (msg.guild_id === null)
			return this.handleDMCommand(msg)

		this.handleCommand(msg)
	}

	async handleMessageReactionAdd(react)
	{
		let wires = this.state.eventWires

		let wire = wires.getAll(EventWireType.PLAYER_ACTION_NEXT, react.message_id)[0]
		if (wire === null)
			return

		let gstate = this.gameStates[wire.idGame]
		if (!gstate.enabled)
			return

		if (wire.idPlayer !== react.user_id)
			return

		if (wire.fired)
			return

		wire.fired = true
		this.saveState()

		let emoji = react.emoji.name
		if ((react.emoji.id ?? null) !== null)
			emoji = `${emoji}:${react.emoji.id}`

		let status
		try
		{
			status = await this.playerStepNext(wire.idGame, wire.idPlayer, emoji)
		}
		catch (e)
		{
			this.rest.createEmbedMessage(wire.idChannel,
					'Onon',
					e.message)
			status = false
		}

		if (!status)
		{
			wire.fired = false
			this.saveState()
			return
		}

		await this.showPlayerStory(wire.idGame, wire.idPlayer, wire.idChannel)
	}

	async nodeGen(id_game)
	{
		let gstate = this.gameStates[id_game]

		gstate.nodesValidated = false

		// 1) Sort
		let nodes = Object.values(gstate.nodes).sort((a, b) => (a.id < b.id) ? -1 : (a.id === b.id) ? 0 : 1)

		// 2) Build the tree
		let tree = new NodeState()
		tree.type = NodeType.ORIGIN

		for (let node of nodes)
		{
			let cb_tree_add_node = (node) => {
				let idx = node.id.lastIndexOf('.')
				let parent_id = ''
				let parent = tree

				node.type = NodeType.UNKNOWN
				if (node.title === ''
						&& node.content === '')
					node.type = NodeType.PASSTHROUGH

				node.idBase = null
				node.idParent = null
				node.idChildren = {}
				node.idNext = null

				if (idx >= 0)
				{
					parent_id = node.id.substring(0, idx)
					parent = gstate.nodes[parent_id] ?? null
				}
				node.idBase = node.id.substring(idx + 1)

				if (parent === null)
				{
					parent = new NodeState(parent_id)
					gstate.nodes[parent.id] = parent
					cb_tree_add_node(parent)
				}

				// Try to set the parent type, error if incompatible
				const parent_types_map = [
					{ rgx: /^(\d+)(?:\[([^\u200D\uFE0F].*)\])?$/,   ptypes: [ NodeType.LINEAR, NodeType.PASSTHROUGH, NodeType.ORIGIN ] },
					{ rgx: /^d([^\u200D\uFE0F].*)$/,                ptypes: [ NodeType.DICE ] },
					{ rgx: /^(_[^\u200D\uFE0F].*)$/,                ptypes: [ NodeType.ORIGIN ] },
					{ rgx: /^(<a?:\w+:\d+>)$/,                      ptypes: [ NodeType.CHOICE ] },
					{ rgx: /^([\u00-\u7f](?:[^\u200D\uFE0F].*|))$/, ptypes: null }, // Any not-emoji-combined ASCII char is rejected
					{ rgx: /^(.+)$/,                                ptypes: [ NodeType.CHOICE ] }
				]
				let expected_ptypes = null

				for (let t of parent_types_map)
				{
					let nsid = node.idBase.match(t.rgx)
					if (nsid !== null)
					{
						node.idBase = nsid[1].replace(/^<a?:(\w+:\d+)>$/, '$1')
						node.customNextEmoji = nsid[2] ?? null
						expected_ptypes = t.ptypes
						break
					}
				}

				if (expected_ptypes === null)
					throw new Error(`Dans le nœud '${node.id}' :\nCommande inconnue : '${node.idBase}'`)

				node.idParent = parent.id
				parent.idChildren[node.idBase] = node.id

				if (!expected_ptypes.includes(parent.type)
						&& parent.type !== NodeType.UNKNOWN)
					throw new Error(`Discordance de types entre le nœud '${node.id}' et son parent '${parent.id}':`
							+ `\n- requis par '${node.id}' : ${expected_ptypes.map(t => NodeType.asString(t)).join(', ')}`
							+ `\n- disponible depuis '${parent.id}' : ${NodeType.asString(parent.type)}`)

				if (parent.type === NodeType.UNKNOWN)
					parent.type = expected_ptypes[0]
			}

			cb_tree_add_node(node)
		}

		if ((gstate.nodes[NodeState.ID_ROOT_NODE] ?? null) === null)
			throw new Error(`Nœud racine '${NodeState.ID_ROOT_NODE}' manquant`)

		// Retype leaf nodes
		for (let node of nodes)
		{
			if (node.type !== NodeType.UNKNOWN)
				continue
	
			if (Object.keys(node.idChildren).length > 0)
				throw new Error(`Le nœud non-catégorisé '${NodeState.ID_ROOT_NODE}' n'est pas un nœud-feuille`)

			node.type = NodeType.LEAF
		}

		gstate.nodesValidated = true

		await this.saveGameState(id_game)
	}

	async showPlayerStory(id_game, id_player, id_channel)
	{
		let wires = this.state.eventWires

		let widx = wires.filterIndex(EventWireType.PLAYER_ACTION_NEXT, { idGame: id_game, idPlayer: id_player })
		wires.removeAll(EventWireType.PLAYER_ACTION_NEXT, ...widx)

		await this.saveState()

		let gstate = this.gameStates[id_game]

		let player = gstate.players[id_player] ?? null
		if (player === null)
			return

		if (player.execStack.length === 0)
			return

		let exec_state = player.execStack[player.execStack.length - 1]
		let node = gstate.nodes[exec_state.idCurNode]
		let ans = await this.rest.createEmbedMessage(id_channel,
				node.title,
				node.content)
		if (!ans.ok)
			return

		ans = await ans.json()

		let wire = new EventWirePlayerActionNextState(id_game, id_player, ans.channel_id, ans.id)
		wires.putAll(wire)

		await this.saveState()

		let emojis = [ exec_state.choice ]

		if (node.type === NodeType.CHOICE)
			emojis = Object.keys(node.idChildren)

		for (let emoji of emojis)
			await this.rest.createReaction(id_channel, ans.id, emoji)
	}

	_autoNextNode(gstate, player)
	{
		let exec_state = player.execStack[player.execStack.length - 1]
		let node = gstate.nodes[exec_state.idCurNode]

		let start_time = Date.now()

		let is_not_wanted_node = true

		while (true)
		{
			let delta = Date.now() - start_time
			if (delta > this.nextNodeMaxDurationMs)
				throw new Error(`L'exécution de l'action a dépassé la limite maximale de temps autorisée`)

			let exec_state = player.execStack[player.execStack.length - 1]
			let node = gstate.nodes[exec_state.idCurNode]

			if (!is_not_wanted_node)
			{
				if (node.type !== NodeType.PASSTHROUGH)
					break
			}
			is_not_wanted_node = false

			// Execute the next act
			
			if (exec_state.idxAct < node.acts.length)
			{
				let act = node.acts[exec_state.idxAct]

				let got_fresh_node = false

				switch (act.type)
				{
					case NodeActionType.CALL:
					{
						if (player.execStack.length >= GameState.CALL_STACK_LIMIT)
							throw new Error(`L'exécution de l'action a dépassé la limite maximale de la pile d'appels (${GameState.CALL_STACK_LIMIT})`)

						exec_state = new ExecState(act.idNode)
						player.execStack.push(exec_state)
						got_fresh_node = true
						break
					}

					case NodeActionType.GOTO:
					{
						exec_state = new ExecState(act.idNode)
						player.execStack = [ exec_state ]
						got_fresh_node = true
						break
					}

					case NodeActionType.ITEM_UNLOCK:
					{
						// TODO
						break
					}

					case NodeActionType.RET:
					{
						player.execStack.pop()
						got_fresh_node = true
						is_not_wanted_node = true
						break
					}
				}

				exec_state.idxAct++

				if (got_fresh_node)
					continue
			}

			// Proceed with unfinished acts before selecting the next node
			if (exec_state.idxAct > 0
					&& exec_state.idxAct < node.acts.length)
			{
				is_not_wanted_node = true
				continue
			}

			// Select the next node

			if (node.type === NodeType.CHOICE
					|| node.type === NodeType.DICE)
			{
				// User-selected child
				node = gstate.nodes[node.idChildren[exec_state.choice]]
			}
			else if (Object.keys(node.idChildren).length > 0)
			{
				// Select the first child
				let keys = Object.keys(node.idChildren).sort((a, b) => (a | 0) - (b | 0))
				node = gstate.nodes[node.idChildren[keys[0]]]
			}
			else
			{
				// Backtrack until a candidate is found
				let nd = node

				while (true)
				{
					let pnd = gstate.nodes[nd.idParent] ?? null
					if (pnd === null)
					{
						// Do an implicit ret if possible
						if (player.execStack.length > 1)
						{
							player.execStack.pop()
							is_not_wanted_node = true

							// At the end of the big loop exec_state will be reset
							// but it does not matter as we already popped it out
							break
						}

						// Return to the beginning
						node = gstate.nodes[NodeState.ID_ROOT_NODE]
						break
					}

					if ((pnd.type === NodeType.LINEAR
								|| pnd.type === NodeType.PASSTHROUGH)
							&& Object.keys(pnd.idChildren).length > 0)
					{
						let keys = Object.keys(pnd.idChildren).sort((a, b) => (a | 0) - (b | 0))
						let kidx = keys.indexOf(nd.idBase)
						if (kidx < keys.length - 1)
						{
							node = gstate.nodes[pnd.idChildren[keys[kidx + 1]]]
							break
						}
					}

					// Keep going
					nd = pnd
				}
			}

			exec_state.reset(node.id)
		}
	}

	async playerProcessRefill(id_game, id_player)
	{
		let gstate = this.gameStates[id_game]

		let player = gstate.players[id_player] ?? null
		if (player === null)
			return

		let now = new Date()
		let now_ts = now.getTime()

		let count = 1
		if (player.lastRefill !== null)
			count = util.countDailySplitsBetween(player.lastRefill, now, gstate.refillSplits)

		if (count <= 0)
			return

		player.movementPoints += count * gstate.refill
		player.lastRefill = now_ts
		this.saveGameState(gstate.id)
	}

	// Runs actions, ... until it reaches a non-passthrough node
	async playerStepNext(id_game, id_player, choice)
	{
		choice ??= null

		let is_root = false
		let gstate = this.gameStates[id_game]

		let player = gstate.players[id_player] ?? null
		if (player === null)
			return false

		this.playerProcessRefill(id_game, id_player)

		if (player.execStack.length === 0)
		{
			player.execStack.push(new ExecState())
			is_root = true
		}

		let exec_state = player.execStack[player.execStack.length - 1]
		let node = gstate.nodes[exec_state.idCurNode]

		// With CHOICE, choice must be decided just-in-time
		if (node.type === NodeType.CHOICE)
		{
			let choices = Object.keys(node.idChildren)

			// Invalid choice
			if (!choices.includes(choice))
				return false

			exec_state.choice = choice
		}

		if (choice !== exec_state.choice)
			return false

		if (!is_root)
		{
			if (player.movementPoints <= 0)
				throw new Error('La fatigue vous empêche de jouer, réessayez plus tard')

			player.movementPoints--
		}

		// Do not skip the root node unless needed
		if (!is_root || node.type === NodeType.PASSTHROUGH)
			this._autoNextNode(gstate, player)

		// Retrieve fresh state
		exec_state = player.execStack[player.execStack.length - 1]
		node = gstate.nodes[exec_state.idCurNode]

		// With DICE, LEAF or LINEAR, choice must be decided right away
		if (node.type === NodeType.DICE)
		{
			let choices = Object.keys(node.idChildren)

			exec_state.choice = choices[Math.random() * choices.length | 0]
		}
		else if (node.type === NodeType.LEAF
				|| node.type === NodeType.LINEAR)
			exec_state.choice = node.customNextEmoji ?? gstate.nextEmoji

		this.saveGameState(gstate.id)
		return true
	}
}
coorepassion.Coorepassion = Coorepassion
