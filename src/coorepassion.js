let logger = await use('concord.logger')

let coorepassion = await ready('coorepassion')

let Coorepassion = await use('coorepassion.Coorepassion')

// logger.setLogLevel(logger.LogLevel.DEBUG)

function boot(token)
{
	let bot = new Coorepassion(token)
	bot.boot()
}

function main()
{
	let coorepassion_submit_elem = document.getElementById('coorepassion_submit')

	coorepassion_submit_elem.addEventListener('click', (ev) => {
		ev.preventDefault()

		let coorepassion_token_elem = document.getElementById('coorepassion_token')
		let token = coorepassion_token_elem.value

		boot(token)
	})

	let url = new URL(document.location)
	let token = url.searchParams.get('token')
	if (token !== null)
		boot(token)
}

if (document.readyState === 'interactive'
		|| document.readyState === 'complete')
	window.setTimeout(main, 0)
else
	document.addEventListener('DOMContentLoaded', main)
