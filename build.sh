#!/bin/bash

set -e

DIR="$(readlink -e "$(dirname "${BASH_SOURCE[0]}")")"

function log()
{
	echo "$@" >&2
}

publicdir='public'
out=out/coorepassion.bundle.js

log "Building prerequisites for '${out}'..."

"${DIR}/concord/build.sh"

log "Building '${out}'..."

"${DIR}/js-bundle/bundle.sh" "${DIR}/src" >"${DIR}/${out}"

log "Built '${out}' succesfully."

log "Generating '${publicdir}'..."

in=(
	'index.html'
	'concord/out/concord.bundle.js'
	'out/coorepassion.bundle.js'
)
publicdir="public"

for i in "${in[@]}"; do
	fname=$(basename "$i")
	log "Install '${i}' => '${publicdir}/${fname}'"
	cp "${DIR}/$i" "${DIR}/${publicdir}/${fname}"
done

log "Generated '${publicdir}' successfully."
